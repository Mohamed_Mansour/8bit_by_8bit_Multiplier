module Multiplier_TOP(A,B,SUM,Carry_Out_Flag);
	input	unsigned [7:0] A,B;
	output	unsigned [15:0] SUM;
	output Carry_Out_Flag;	
	wire	[15:0] OUT1;
	wire	[13:0] OUT2;
	wire	[11:0] OUT3;
	wire	[9:0] OUT4;
	wire	[7:0] OUT5;
	wire 	[15:0] Num2;
	wire 	[11:0] temp1;
	wire 	[9:0] temp2,temp4;
	wire 	[8:0] temp3,temp5;
	genvar i;
	
	assign Num2[1:0] = 0;
	assign Num2[2] = OUT2[0];
	assign Num2[3] = OUT2[1];			
	PP		PP1		(.Y(A[7:0]),.X(B[7:0]),.OUT1(OUT1[15:0]),.OUT2(OUT2[13:0]),.OUT3(OUT3[11:0]),.OUT4(OUT4[9:0]),.OUT5(OUT5[7:0]));
	// to generate B4
	FA		FA1		(.A(OUT2[2]),.B(OUT3[0]),.Cin(0),.Sum(Num2[4]),.Carry_Out(temp1[0]));
	// to generate B5
	FA		FA2		(.A(OUT2[3]),.B(OUT3[1]),.Cin(temp1[0]),.Sum(Num2[5]),.Carry_Out(temp1[1]));
	// first raw contains 10 FAs 
	for (i=0;i<10;i=i+1) begin
	    FA      FA3     (.A(OUT2[i+4]),.B(OUT3[i+2]),.Cin(OUT4[i]),.Sum(temp2[i]),.Carry_Out(temp1[i+2]));
	end
	// to generate B6
	FA		FA4		(.A(temp2[0]),.B(temp1[1]),.Cin(0),.Sum(Num2[6]),.Carry_Out(temp4[0]));
	// first element in the second raw
	FA		FA5		(.A(temp2[1]),.B(temp1[2]),.Cin(0),.Sum(temp3[0]),.Carry_Out(temp4[1]));
	// second raw contains another 8 FA and total FAs of 10
	for (i=0;i<8;i=i+1) begin
	    FA      FA6     (.A(temp2[i+2]),.B(temp1[i+3]),.Cin(OUT5[i]),.Sum(temp3[i+1]),.Carry_Out(temp4[i+2]));
	end
	// final stage 
	// first element of the last raw
	FA		FA7		(.A(temp3[0]),.B(temp4[0]),.Cin(0),.Sum(Num2[7]),.Carry_Out(temp5[0]));
	// another 8 FAs in the last raw
	for (i=1;i<9;i=i+1)
	    FA      FA8     (.A(temp3[i]),.B(temp4[i]),.Cin(temp5[i-1]),.Sum(Num2[i+7]),.Carry_Out(temp5[i]));
	end
	// Using Sklansky Adder to sum the merging vector 
	Sklansky	Sklansky1	(.A(OUT1[15:0]),.B(Num2[15:0]),.Cin(0),.OUT(SUM[15:0]),.Carry_Out_Flag(Carry_Out_Flag));	
	
endmodule
